#!/usr/bin/env python

import math
import random
import pygame as pg

# Constants
WIDTH = 640
ROWS = 3

# Colors
BACKGROUND = (26, 27, 38)
FOREGROUND = (192, 202, 245)
HIGHLIGHT = (40, 46, 66)

# Initialize Pygame
pg.init()
win = pg.display.set_mode((WIDTH, WIDTH))
pg.display.set_caption("Noughts and Crosses")

# Load Images
X_IMAGE = pg.transform.scale(pg.image.load("images/x.png"), (WIDTH // 4, WIDTH // 4))
O_IMAGE = pg.transform.scale(pg.image.load("images/o.png"), (WIDTH // 4, WIDTH // 4))

# Fonts
font = pg.font.SysFont('Arial', WIDTH // 15)
button_font = pg.font.SysFont('Arial', 30)

# Logic for Computer Move
def computer_move(grid_array):
    available_moves = [(i, j) for i in range(ROWS) for j in range(ROWS) if grid_array[i][j][3]]
    corners = [(0, 0), (0, 2), (2, 0), (2, 2)]
    available_corners = [move for move in available_moves if move in corners]

    if available_corners:
        return random.choice(available_corners)
    return random.choice(available_moves)


def draw_grid():
    # Calculating the gap between each line in the grid
    gap = WIDTH // ROWS

    # Drawing horizontal and vertical lines to create the grid
    for i in range(1, ROWS):
        # Drawing horizontal lines
        pg.draw.line(win, HIGHLIGHT, (0, i * gap), (WIDTH, i * gap), 3)
        # Drawing vertical lines
        pg.draw.line(win, HIGHLIGHT, (i * gap, 0), (i * gap, WIDTH), 3)


def initialize_grid():
    # Calculating the distance to the center of the cell
    dis_to_cen = WIDTH // ROWS // 2

    # Initializing the game grid as a 2D list
    grid_array = [[None, None, None] for _ in range(ROWS)]

    # Iterating through each cell in the grid
    for i in range(ROWS):
        for j in range(ROWS):
            # Calculating the coordinates of the center of the current cell
            x = dis_to_cen * (2 * j + 1)
            y = dis_to_cen * (2 * i + 1)
            # Storing the coordinates and an empty string to represent the cell being unmarked and available for play
            grid_array[i][j] = (x, y, "", True)

    # Returning the initialized game grid
    return grid_array


def click(grid_array, x_turn, o_turn, images, playing_against_computer, turn, leaderboard_text):

    # Getting the current position of the mouse
    m_x, m_y = pg.mouse.get_pos()

    # Iterating through each cell in the game grid
    for i in range(ROWS):
        for j in range(ROWS):
            x, y, char, can_play = grid_array[i][j]

            # Calculating the distance between the mouse click and the center of the cell
            dis = math.sqrt((x - m_x) ** 2 + (y - m_y) ** 2)

            # Checking if the mouse click is within the cell and the cell is available for playing
            if dis < WIDTH // ROWS // 2 and can_play:
                # If it's X's turn, mark the cell with X and switch turns to O
                if x_turn:
                    images.append((x, y, X_IMAGE))
                    x_turn = False
                    o_turn = True
                    grid_array[i][j] = (x, y, 'x', False)
                # If it's O's turn, mark the cell with O and switch turns to X
                elif o_turn:
                    images.append((x, y, O_IMAGE))
                    x_turn = True
                    o_turn = False
                    grid_array[i][j] = (x, y, 'o', False)

                render(images)
                render_leaderboard(leaderboard_text)
                pg.display.update()
                if playing_against_computer and o_turn:
                    available_moves = [(i, j) for i in range(ROWS) for j in range(ROWS) if grid_array[i][j][3]]
                    if available_moves:
                        pg.time.delay(1000)
                        move = computer_move(grid_array)
                        i, j = move
                        images.append(
                            (grid_array[i][j][0], grid_array[i][j][1], O_IMAGE))
                        x_turn = True
                        o_turn = False
                        grid_array[i][j] = (
                            grid_array[i][j][0], grid_array[i][j][1], 'o', False)
                        turn += 1
                turn += 1
    # Returning the updated states of the game
    return x_turn, o_turn, images, turn


def has_won(grid_array):
    # Checking rows
    for row in range(len(grid_array)):
        if (grid_array[row][0][2] == grid_array[row][1][2] == grid_array[row][2][2]) and grid_array[row][0][2] != "":
            display_message(grid_array[row][0][2].upper() + " has won!")
            return True

    # Checking columns
    for col in range(len(grid_array)):
        if (grid_array[0][col][2] == grid_array[1][col][2] == grid_array[2][col][2]) and grid_array[0][col][2] != "":
            display_message(grid_array[0][col][2].upper() + " has won!")
            return True

    # Checking main diagonal
    if (grid_array[0][0][2] == grid_array[1][1][2] == grid_array[2][2][2]) and grid_array[0][0][2] != "":
        display_message(grid_array[0][0][2].upper() + " has won!")
        return True

    # Checking reverse diagonal
    if (grid_array[0][2][2] == grid_array[1][1][2] == grid_array[2][0][2]) and grid_array[0][2][2] != "":
        display_message(grid_array[0][2][2].upper() + " has won!")
        return True

    return False


def has_drawn(turn):
    if turn == 9:
        # If all cells are filled and no winner is determined, display the draw message
        display_message("It's a draw!")
        # Return True to indicate that the game has ended in a draw
        return True
    return False


def display_message(content):
    # Fill the window with BG color
    win.fill(BACKGROUND)

    # Render the message content using the font with the color FOREGROUND
    end_text = font.render(content, 1, FOREGROUND)

    # Position the message content at the center of the window
    win.blit(end_text, ((WIDTH - end_text.get_width()) // 2, (WIDTH - end_text.get_height()) // 2))

    # Update the display to show the rendered message
    pg.display.update()

    pg.time.delay(1500)


def render(images):
    # Fill the window with BG color
    win.fill(BACKGROUND)

    # Draw the grid
    draw_grid()

    # Loop through each image in the list of images
    for image in images:
        # Unpack into separate variables
        x, y, IMAGE = image

        # Position the image at the specified coordinates relative to its size
        win.blit(IMAGE, (x - IMAGE.get_width() // 2, y - IMAGE.get_height() // 2))

def render_leaderboard(leaderboard_text):
    # Render the leaderboard text
    leaderboard_rendered = font.render(leaderboard_text, 1, FOREGROUND)
    win.blit(leaderboard_rendered, ((WIDTH - leaderboard_rendered.get_width()) // 2, 10))

    # Update the display to show the images
    pg.display.update()


def main():
    images = []
    x_turn = True
    o_turn = False
    grid_array = initialize_grid()
    turn = 0
    playing_against_computer = False
    x_score = 0
    o_score = 0

    leaderboard_text = "Player X: {}  Player O: {}".format(x_score, o_score)

    button_text_player = button_font.render("Player", True, BACKGROUND)
    button_text_computer = button_font.render("Computer", True, BACKGROUND)
    player_button = pg.Rect(150, 200, 150, 50)
    computer_button = pg.Rect(350, 200, 150, 50)

    running = True
    while running:
        for event in pg.event.get():
            if event.type == pg.QUIT:
                running = False
            if event.type == pg.MOUSEBUTTONDOWN:
                mouse_pos = event.pos
                if player_button.collidepoint(mouse_pos):
                    playing_against_computer = False
                    running = False
                elif computer_button.collidepoint(mouse_pos):
                    playing_against_computer = True
                    running = False

        win.fill(BACKGROUND)
        pg.draw.rect(win, FOREGROUND, player_button)
        pg.draw.rect(win, FOREGROUND, computer_button)
        win.blit(button_text_player, (180, 210))
        win.blit(button_text_computer, (360, 210))

        pg.display.flip()

    running = True
    while running:
        for event in pg.event.get():
            if event.type == pg.QUIT:
                running = False
            if event.type == pg.MOUSEBUTTONDOWN:
                x_turn, o_turn, images, turn = click(grid_array, x_turn, o_turn, images, playing_against_computer, turn, leaderboard_text)

        if has_won(grid_array):
            if not x_turn:
                x_score += 1
            else:
                o_score += 1
            leaderboard_text = f"Player X: {x_score}  Player O: {o_score}"
            render(images)
            render_leaderboard(leaderboard_text)
            images = []
            turn = 0
            # Reset turns so x goes first again
            x_turn = True
            o_turn = False
            grid_array = initialize_grid()

        elif has_drawn(turn):
            render(images)
            render_leaderboard(leaderboard_text)
            images = []
            turn = 0
            # Reset turns so x goes first again
            x_turn = True
            o_turn = False
            grid_array = initialize_grid()

        else:
            render(images)
            render_leaderboard(leaderboard_text)

    pg.quit()


if __name__ == '__main__':
    main()
